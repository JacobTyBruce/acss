import Vue from "vue";
import Vuex from "vuex";
import { productList } from "../assets/db.js";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    productList: productList
  },
  mutations: {},
  actions: {},
  modules: {}
});

export default store;